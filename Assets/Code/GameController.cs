using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Text pointsText;
    public int pointsToWin;
    public GameObject endGamePanel;


    private int points;

    public static GameController instance;

    private void Awake()
    {   
        if(instance == null)
        {
            instance = this;
        }
        Time.timeScale = 1;
    }

    public void AddPoint(int amount)
    {
        points += amount;
        pointsText.text = "Punkty : " + points;
        CheckIfWin();
    }

    private void CheckIfWin()
    {
        if(points >= pointsToWin)
        {
            endGamePanel.SetActive(true);
            Time.timeScale = 0;
        }
    }


    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
