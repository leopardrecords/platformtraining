using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    public Rigidbody rb;

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        Vector3 moveDirection = new Vector3(horizontal*speed*Time.deltaTime,0,0);
        transform.Translate(moveDirection);

        if (IsGrounded())
        {
            if(Input.GetButtonDown("Jump") == true)
            {
                rb.AddForce(Vector3.up * jumpForce,ForceMode.Impulse);
            }
        }
    }

    private bool IsGrounded()
    {
        float distanceToGround = GetComponent<Collider>().bounds.extents.y;
        bool isGrounded = Physics.Raycast(transform.position,Vector3.down,distanceToGround + 0.15f);
        Debug.DrawRay(transform.position,Vector3.down * (distanceToGround + 0.15f), Color.red);
        return isGrounded;
    }
}